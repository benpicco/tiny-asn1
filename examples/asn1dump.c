#include "tiny-asn1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

void print_hex(uint8_t* data, unsigned int len)
{
  unsigned int count = 0;
  unsigned int blockCount = 0;
  while(count < len) {
    printf("%02x ", data[count]);
    ++count;
    ++blockCount;
    if(blockCount == 4)
      printf("  ");
    if(blockCount == 8) {
      printf("\n");
      blockCount = 0;
    }
  }
  printf("\n");
}


void print_asn1(asn1_tree* list, int depth)
{
  printf("d=%d, Tag: %02x, len=%"PRIu32"\n", depth, list->type, list->length);
  if(list->child == NULL) {
    printf("Value:\n");
    print_hex(list->data, list->length);
  } else {
    print_asn1(list->child, depth+1);
  }

  if(list->next != NULL)
    print_asn1(list->next, depth);
}


int main(int argc, char** argv)
{
	if(argc <= 1) {
		fprintf(stderr, "Please specify an input file.\n");
		exit(-1);
	}

	FILE* file = fopen(argv[1], "r");
	if(file == NULL) {
		fprintf(stderr, "Could not open file.\n");
		exit(-2);
	}

	uint32_t file_size = 0;
	while(EOF != fgetc(file))
		++file_size;
	
	rewind(file);

	uint8_t* cms_data = malloc(file_size);
	if(cms_data == NULL) {
		fprintf(stderr, "Could not allocate memory.\n");
		exit(-3);
	}
	uint32_t file_pos = 0;
	int byte_read = fgetc(file);
	while(byte_read != EOF && file_pos < file_size) {
		cms_data[file_pos++] = (uint8_t)byte_read;
		byte_read = fgetc(file);
	}
	fclose(file);


	int32_t asn1_object_count = der_object_count(cms_data, file_size);
	if(asn1_object_count < 0) {
		fprintf(stderr, "ERROR: Could not calculate the number of Elements within the data.\n");
		free(cms_data);
    	return 1;
  	}

	asn1_tree* asn1_objects = (asn1_tree*)(malloc(sizeof(asn1_tree) * asn1_object_count));
  	if(asn1_objects == NULL){
		fprintf(stderr, "ERROR: Could not allocate the memory for the ASN.1 objects.\n");
		free(cms_data);
    	return 1;
  	}

  	asn1_tree cms;

  	if(der_decode(cms_data, file_size, &cms, asn1_objects, asn1_object_count) < 0) {
		fprintf(stderr, "ERROR: Could not parse the data.\n");
		free(cms_data);
    	return 1;
  	}

  	//Dump the data
  	print_asn1(&cms, 0);


  free(asn1_objects);
  free(cms_data);

  return 0;
}
